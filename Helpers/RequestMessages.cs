namespace Lib.Infra.ExternalService.Helpers 
{ 
    public static class RequestMessages 
    { 
        public const string Padrao = "A requisição falhou e não retornou erros."; 
        public const string SerializingError = "Erro ao deserializar a resposta."; 
        public const string BadRequestError = "Parâmetros da requisição são inválidas."; 
        public const string InternalServerError = "Erro interno do serviço."; 
        public const string ResourceNotFound = "Url não encontrada"; 
        public const string TokenInvalido = "Requisição não autorizada."; 
        public const string Timeout = "Requisição ultrapassou tempo limite de resposta."; 
        public const string FalhaComunicacao = "Falha na comunicação com o server."; 
        public const string ServicoIndisponivel = "O serviço esta indisponível"; 
    } 
} 
