namespace Lib.Infra.ExternalService.Configurations 
{ 
    public class ProxyServer 
    { 
        public string Endereco { get; set; } 
        public int? Porta { get; set; } 
        public bool Ativo { get; set; } 
    } 
} 
