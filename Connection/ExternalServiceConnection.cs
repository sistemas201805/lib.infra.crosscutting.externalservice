using System; 
using System.Net; 
using RestSharp; 
using Lib.Infra.ExternalService.Helpers; 
using System.Collections.Generic; 
using Lib.Infra.ExternalService.Configurations; 
using Microsoft.Extensions.Options; 
  
namespace Lib.Infra.ExternalService.Connection 
{ 
    public abstract class ExternalServiceConnection 
    { 
        protected string baseUrl { get; set; } 
        protected ProxyServer proxy { get; set; } 
        protected AuthSSL autySSL { get; set; } 
  
        public ExternalServiceConnection( 
            string _baseUrl, 
            IOptions<ProxyServer> _proxy, 
            IOptions<AuthSSL> _autySSL 
            ) 
        { 
            baseUrl = _baseUrl; 
            proxy = _proxy.Value; 
            autySSL = _autySSL.Value; 
        } 
  
        private int MinutesToTimeout(int minutes) 
        { 
            return minutes * 60 * 1000; 
        } 
  
        private string ConfigurarProxy() 
        { 
            return proxy.Ativo ? string.Format("{0}{1}", proxy.Endereco, string.IsNullOrEmpty(proxy.Porta.ToString()) ? null : ":" + proxy.Porta.ToString()) : null; 
        } 
  
        private bool ConfigurarAuthSSL() 
        { 
            return autySSL.Ignore.HasValue ? autySSL.Ignore.Value : false; 
        } 
  
        public string HttpGetData(string recurso, dynamic body, string authToken) 
        { 
            string auxRecurso = baseUrl + "/" + recurso + "/" + body; 
            var request = new RestRequest(auxRecurso, Method.GET); 
            request.AddHeader("Authorization", authToken); 
  
            string responseContent = null; 
            responseContent = Execute(request, auxRecurso); 
            return responseContent; 
        } 
  
        public string HttpPostData(string recurso, dynamic body, string authToken) 
        { 
            string auxRecurso = baseUrl + "/" + recurso; 
            var request = new RestRequest(auxRecurso, Method.POST); 
            request.AddJsonBody(body); 
            request.AddHeader("Authorization", authToken); 
  
            string responseContent = null; 
            responseContent = Execute(request, auxRecurso); 
            return responseContent; 
        } 
  
        private string Execute(RestRequest request, string recurso) 
        { 
            if (ConfigurarAuthSSL()) 
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true; 
  
            var _proxy = ConfigurarProxy(); 
            var uri = new Uri(recurso, UriKind.Absolute); 
            var client = new RestClient(uri) 
            { 
                Timeout = MinutesToTimeout(5), 
                Proxy = string.IsNullOrEmpty(_proxy) ? null : new WebProxy(_proxy) 
            }; 
  
            var response = client.Execute(request); 
  
            VerifyResponseStatus(response, recurso); 
  
            return response.Content; 
        } 
  
        private void VerifyResponseStatus(IRestResponse response, string auxRecurso) 
        { 
            if (response.StatusCode == HttpStatusCode.OK && 
                string.IsNullOrEmpty(response.ErrorMessage) && 
                response.ErrorException == null) 
                return; 
  
            var messageList = new List<string>(); 
            if (response.StatusCode == HttpStatusCode.Unauthorized) 
                messageList.Add(RequestMessages.TokenInvalido); 
            else if (response.StatusCode == HttpStatusCode.ServiceUnavailable) 
                messageList.Add(RequestMessages.ServicoIndisponivel); 
            else if (response.StatusCode == HttpStatusCode.BadRequest) 
                messageList.Add(RequestMessages.BadRequestError); 
            else if (response.StatusCode == HttpStatusCode.InternalServerError) 
                messageList.Add(RequestMessages.InternalServerError); 
            else if (response.StatusCode == HttpStatusCode.RequestTimeout) 
                messageList.Add(RequestMessages.Timeout); 
            else 
            { 
                if (!string.IsNullOrEmpty(response.ErrorMessage)) 
                    messageList.Add($"Error: {response.ErrorMessage}"); 
  
                if (response.ErrorException != null) 
                    messageList.Add($"Error: {response.ErrorException.Message}"); 
            } 
  
            Console.WriteLine($"Erro ao tentar obter o recurso {auxRecurso}."); 
            foreach (var item in messageList) 
            { 
                Console.WriteLine(item); 
            } 
            Console.WriteLine($"StatusCode {response.StatusCode}"); 
        } 
    } 
} 
